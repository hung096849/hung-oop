<?php
require('../dao/ProductDao.php');
class ProductDaoDemo extends ProductDao
{
    /**
     * Test insert row to product table
     * 
     * @param $row
     * @return array
     */
    public function insertTest($row)
    {
        $this->insert($row);
    }

    /**
     * Test select row from product table
     * 
     * @param $row
     * @return array
     */
    public function selectTest($row)
    {
        print_r($this->insert($row));
    }

    /**
     * Test update row to category table
     * 
     * @param $row
     * @return array
     */
    public function updateTest($row)
    {
        print_r($this->update($row));
    }

    /**
     * Test delete row to category table
     * 
     * @param $row
     * @return array
     */
    public function deleteTest($row)
    {
        print_r($this->delete($row));
    }
}

$product = new product(1, 'iphone',1);
$product1 = new product(2, 'samsung',1);
$productDaoDemo = new productDaoDemo();
$productDaoDemo->insertTest($product);
$productDaoDemo->selectTest($product);
$productDaoDemo->updateTest($product1);
$productDaoDemo->deleteTest($product1);
